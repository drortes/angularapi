import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http:Http) { }

  getBooks(){
    let url = 'https://www.googleapis.com/books/v1/volumes?q=harrypotter';

    return this.http.get(url).pipe(
      map(res=> res.json() )
    );
  }
}
