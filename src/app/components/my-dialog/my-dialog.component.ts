import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";


@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.css']
})
export class MyDialogComponent implements OnInit {
  book: any;
  updatedBook:any;

  constructor(private dialogRef: MatDialogRef<MyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
   this.updatedBook = {
     id:data.id,
     title:data.title,
     authors:data.authors,
     publishedDate: data.publishedDate,
     description: data.description
   }
      this.book = data;

  }

  ngOnInit() {

  }
  onOkclick(){
    this.dialogRef.close(this.updatedBook);
  }
  onCancelclick(){
    this.dialogRef.close();
  }

}
