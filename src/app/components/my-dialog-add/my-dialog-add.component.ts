import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material";


const guid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

@Component({
  selector: 'app-my-dialog-add',
  templateUrl: './my-dialog-add.component.html',
  styleUrls: ['./my-dialog-add.component.css']
})


export class MyDialogAddComponent implements OnInit {
  addedBook: any
  constructor(private dialogRef: MatDialogRef<MyDialogAddComponent>) {
    this.addedBook = {
      id: guid(),
      title: '',
      authors: [],
      publishedDate: '',
      description: ''
    }
  }

  ngOnInit() {
  }

  onOkclick(){
    
    this.dialogRef.close(this.addedBook);
  
  }
  onCancelclick(){
    
    this.dialogRef.close();
  
  }

}
