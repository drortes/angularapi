import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDialogAddComponent } from './my-dialog-add.component';

describe('MyDialogAddComponent', () => {
  let component: MyDialogAddComponent;
  let fixture: ComponentFixture<MyDialogAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDialogAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDialogAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
