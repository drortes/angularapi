import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { MyDialogComponent } from '../my-dialog/my-dialog.component';
import { MatDialog } from '@angular/material';
import { MyDialogDeleteComponent } from '../my-dialog-delete/my-dialog-delete.component';
import { MyDialogAddComponent } from '../my-dialog-add/my-dialog-add.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  books: any;
  constructor(private bookService: BooksService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.bookService
      .getBooks()
      .subscribe(books => {
        let booksList = [];
        for (var i = 0; i < books.items.length; i++) {
          let book = {
            id: books.items[i].id,
            title: books.items[i].volumeInfo.title,
            authors: books.items[i].volumeInfo.authors,
            publishedDate: books.items[i].volumeInfo.publishedDate,
            description: books.items[i].volumeInfo.description
          };
          booksList.push(book);
        }
        return this.books = booksList;
      }

      );
  }

  openDialog(book: object): void {

    let dialogRef = this.dialog.open(MyDialogComponent, {
      width: '250px',
      data: book
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        for (var i = 0; i < this.books.length; i++) {
          if (this.books[i].id == result.id) {
            this.books[i] = result;
          }
        }
      }
    });
  }
  openDeleteDialog(book: object): void {
    let dialogRef = this.dialog.open(MyDialogDeleteComponent, {
      width: '250px',
      data: book
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        let bookToDelteIndex = -1;
        for (var i = 0; i < this.books.length; i++) {
          if (this.books[i].id == result.id) {
            bookToDelteIndex = i;
          }
        }
        if (bookToDelteIndex != -1) this.books.splice(bookToDelteIndex, 1);
      }
    });
  }
  OpenAddDialog(): void {
    let dialogRef = this.dialog.open(MyDialogAddComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.books.push(result);
      }
    });
  }
}
