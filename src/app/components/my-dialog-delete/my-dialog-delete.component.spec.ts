import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDialogDeleteComponent } from './my-dialog-delete.component';

describe('MyDialogDeleteComponent', () => {
  let component: MyDialogDeleteComponent;
  let fixture: ComponentFixture<MyDialogDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyDialogDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyDialogDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
