import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";


@Component({
  selector: 'app-my-dialog-delete',
  templateUrl: './my-dialog-delete.component.html',
  styleUrls: ['./my-dialog-delete.component.css']
})
export class MyDialogDeleteComponent implements OnInit {
  book: any;
  constructor(private dialogRef: MatDialogRef<MyDialogDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.book = data;
  }

  ngOnInit() {

  }
  onOkclick() {
    this.dialogRef.close(this.book);
  }
  onCancelclick() {
    this.dialogRef.close();
  }

}
