import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import{MatButtonModule,MatCheckboxModule,MatTableModule,MatDialogModule, MatIconModule} from '@angular/material';
import { MyDialogComponent } from './components/my-dialog/my-dialog.component';
import { MyDialogDeleteComponent } from './components/my-dialog-delete/my-dialog-delete.component';
import { MyDialogAddComponent } from './components/my-dialog-add/my-dialog-add.component';


@NgModule({
  declarations: [
    
    AppComponent,
    MainComponent,
    FooterComponent,
    HeaderComponent,
    MyDialogComponent,
    MyDialogDeleteComponent,
    MyDialogAddComponent,


  ],
  imports: [
    BrowserModule,
    MatButtonModule, MatCheckboxModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    MatDialogModule


  ],
  entryComponents:[MyDialogComponent,MyDialogDeleteComponent,MyDialogAddComponent],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
